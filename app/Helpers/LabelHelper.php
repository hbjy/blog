<?php

namespace App\Helpers;

class LabelHelper
{
    /**
     * Converts a 'name' field (i.e. 'admin_access') into a label (i.e. 'Admin access')
     *
     * @param string A name field
     *
     * @return string
     */
    public static function nameToLabel( string $name )
    {
        return ucwords( implode( ' ', explode( '_', $name ) ) );
    }
}
