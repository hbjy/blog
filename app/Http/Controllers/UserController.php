<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function display_login()
    {
        return view('account.login');
    }

    public function login( Request $request )
    {
        $credentials = $request->only([ 'email', 'password' ]);

        $errors = [];

        $email = $credentials['email'];
        $password = $credentials['password'];

        if(Auth::attempt(['email' => $email, 'password' => $password])) {
            $user = Auth::user();
            $perms = $user->role->getPermissionNames();

            if( in_array( 'admin', $perms ) ) {
                return redirect()->intended('dashboard');
            } else {
                Auth::logout();
                return redirect()->intended('login')->with([
                    'error' => 'Your account does not have permission to use the admin panel.'
                ]);
            }
        }

        return redirect()->intended('login')->with([
            'error' => 'The account \'' . $email . '\' does not exist in our records.'
        ]);
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->intended('/');
    }
}
