<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function getPermissions()
    {
        $perms = explode( ',', $this->permissions);
        $perms_arr = [];
        foreach( $perms as $perm )
        {
            $permission = Permission::where('id', $perm)->first();
            if($permission)
            {
                $perms_arr[] = $permission;
            }
        }
        return $perms_arr;
    }

    public function getPermissionNames()
    {
        $perms = $this->getPermissions();
        $names = [];
        foreach( $perms as $perm )
        {
            $names[] = $perm->name;
        }
        return $names;
    }
}
