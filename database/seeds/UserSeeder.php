<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User;
        $admin->username = 'Administrator';
        $admin->email = 'admin@something.com';
        $admin->password = Hash::make( 'password' );
        $admin->role_id = 1;
        $admin->save();
    }
}
