<?php

use Illuminate\Database\Seeder;

use App\Helpers\LabelHelper;

use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'administrator' => '1',
            'editor' => '5,6,7,8,9,10',
            'viewer' => '8'
        ];

        foreach ( $roles as $role => $perms )
        {
            Role::create([
                'name' => $role,
                'label' => LabelHelper::nameToLabel($role),
                'permissions' => $perms
            ]);
        }
    }
}
