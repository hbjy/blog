<?php

use Illuminate\Database\Seeder;

use App\Helpers\LabelHelper;

use App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'admin',
            'create_user',
            'edit_user',
            'delete_user',
            'create_post',
            'edit_post',
            'delete_post',
            'create_comment',
            'edit_comment',
            'delete_comment'
        ];

        foreach ( $permissions as $perm )
        {
            Permission::create([
                'name' => $perm,
                'label' => LabelHelper::nameToLabel($perm)
            ]);
        }
    }
}
