<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home')->name('home');

Route::get('/login', 'UserController@display_login')->name('login');
Route::post('/login', 'UserController@login');
Route::get('/logout', 'UserController@logout');

Route::get('/admin', 'AdminController@dashboard')->middleware('auth')->name('dashboard');
