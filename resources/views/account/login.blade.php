@extends('layouts.auth')

@section('title', 'Login')

@section('content')


<div class="wrap" style="min-width: 330px; max-width: 330px">
    @if( session('error') )
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    <div class="card mt-4 shadow border-0 rounded-0">
        <div class="card-body">
            <h3 class="mb-2">Login to {{ config('app.name') }}</h3>
            <form action="/login" method="post" class="needs-validation" novalidate>
                @csrf
                <div class="form-group">
                    <label for="email_field">Email Address</label>
                    <input
                        type="email"
                        name="email"
                        id="email_field"
                        placeholder="john.doe@google.com"
                        class="form-control"
                        required />
                    <div class="invalid-feedback">
                        Please include your email.
                    </div>
                </div>

                <div class="form-group mb-4">
                    <label for="password_field">Password</label>
                    <input
                        type="password"
                        name="password"
                        id="password_field"
                        placeholder="Password"
                        class="form-control"
                        required />
                    <div class="invalid-feedback">
                        Please include your password.
                    </div>
                </div>

                <button type="submit" class="btn btn-primary w-100">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection
