<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') - {{ config('app.name') }}</title>

    <!-- Blog CSS -->
    <link href="/css/app.css" rel="stylesheet">
</head>
<body class="auth d-flex justify-content-around align-items-center">

    @yield('content')

<script src="/js/app.js"></script>
</body>
</html>
