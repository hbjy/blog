<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') - {{ config('app.name') }}</title>

    <!-- Playfair Display (Font) -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">

    <!-- Blog CSS -->
    <link href="/css/app.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        @include('components.header')

        @yield('content')
    </div><!-- /.row -->

    @include('components.footer')

    <!-- Blog JS -->
    <script src="/js/app.js"></script>
</body>
</html>
